$(document).ready(function() {

  loadUsers();

  //load users
  function loadUsers() {
    tableBody = $("table tbody");
    tableBody.empty();
    $.ajax({
      "url": "http://localhost:8080/ws_get_user.php",
      "type": "GET",
      "dataType": "json",
    }).done(function(data) {
      data.forEach((element) => {
        rowTable = `<tr>
          <td>${element.id}</td>
          <th scope="row">${element.fullname}</th>
          <td>${element.email}</td>
          <td>${element.openid}</td>
<td><a class="btn btn-link" href="comments.html?id=${element.id}" >Show Comments</a></td>
<td><button id="showUpdateUserModal" type="button" class="btn btn-primary" userId="${element.id}">Update</button></td>
<td><button id="deleteUser" type="button" class="btn btn-danger" userId="${element.id}">Delete</button></td>
        </tr>
        `;
        tableBody.append(rowTable);
      });
    }).fail(function(jqXHR, textStatus) {
    });
  }


  $("#addUser").click(function(e) {
    e.preventDefault();
    var fullname = $('#aInputFullName').val();
    var email = $('#aInputEmail').val();
    var openid = $('#aInputOpenId').val();
    var password = $('#aInputPassword').val();

    $.ajax({
      "url": "http://localhost:8080/ws_post_user.php",
      "type": "POST",
      "dataType": "json",
      "data": JSON.stringify({
        "fullname": fullname,
        "email": email,
        "openid": openid,
        "pass": password
      }),
    }).done(function(data) {
      $("#addUserModal").modal('hide');
      $("#alert-ok").text(data.message);
      $("#alert-ok").show();
      $("#alert-ok").fadeTo(2000, 500).slideUp(500, function() {
        $("#alert-ok").hide();
        $(':input').val('');
      });
      loadUsers();
    }).fail(function(jqXHR, textStatus) {
      console.log();
      $("#addUserModal").modal('hide');
      $("#alert-error").text(JSON.parse(jqXHR.responseText).message);
      $("#alert-error").show();
      $("#alert-error").fadeTo(2000, 500).slideUp(500, function() {
        $("#alert-error").hide();
      });
      loadUsers();

    });
  });

  $(document).on("click", '#showUpdateUserModal', function(e) {
    e.preventDefault();
    $.ajax({
      "url": "http://localhost:8080/ws_get_user.php?id=" + $(this).attr("userId"),
      "type": "GET",
      "dataType": "json",
    }).done(function(data) {
      $('#uInputId').val(data.id);
      $('#uInputFullName').val(data.fullname);
      $('#uInputEmail').val(data.email);
      $('#uInputOpenId').val(data.openid);
      $('#uInputPassword').val(data.pass);
    }).fail(function(jqXHR, textStatus) {
    });
    $("#updateUserModal").modal('show');
  });

  $(document).on("click", "#updateUser", function(e) {
    e.preventDefault();
    console.log("updateUser");
    $.ajax({
      "url": "http://localhost:8080/ws_put_user.php",
      "type": "PUT",
      "dataType": "json",
      "data": JSON.stringify({
        "id": $('#uInputId').val(),
        "fullname": $('#uInputFullName').val(),
        "email": $('#uInputEmail').val(),
        "openid": $('#uInputOpenId').val(),
        "pass": $('#uInputPassword').val(),
      }),
    }).done(function(data) {
      $("#updateUserModal").modal('hide');
      $("#alert-ok").text(data.message);
      $("#alert-ok").show();
      $("#alert-ok").fadeTo(2000, 500).slideUp(500, function() {
        $("#alert-ok").hide();
      });
      loadUsers();
    });
  });

  $(document).on("click", "#deleteUser", function(e) {
    var deleteUser = confirm("Are you sure?");
    if (deleteUser) {
      $.ajax({
        "url": "http://localhost:8080/ws_delete_user.php?id=" + $('#deleteUser').attr("userId"),
        "type": "DELETE",
        "dataType": "json",
      }).done(function(data) {
        loadUsers();
      });
    }
  });

});
