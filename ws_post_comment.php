<?php
require("./conDB.php");

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$dInput = json_decode(file_get_contents("php://input"));

$qry="INSERT INTO user_comment (user, coment_text, likes) VALUES ('$dInput->user', '$dInput->comment_text', '$dInput->likes')";

if (mysqli_query($conn, $qry)) {
  http_response_code(200);
  echo json_encode(array("message" => "Comment was created."));
} else {
  http_response_code(500);
  echo json_encode(array("message" => "Unable to create comment."));
}
