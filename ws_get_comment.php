<?php
require("./conDB.php");

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$dInput = json_decode(file_get_contents("php://input"));

if (isset($_GET["id"])) {
  $qry = "SELECT * FROM user_comment where id = '" . $_GET['id'] . "'";
  if ($result = mysqli_query($conn, $qry)) {
    $row = mysqli_fetch_assoc($result);
    if (!$row) {
      http_response_code(400);
      echo json_encode(array("message" => "Comment does not exist."));
      return;
    }
  }
  http_response_code(200);
  echo json_encode(mysqli_fetch_array(mysqli_query($conn, $qry)));
} else if (isset($_GET["user"])) {
  $qry = "SELECT * FROM user_comment where  user = '" . $_GET['user'] . "'";
  if ($result = mysqli_query($conn, $qry)) {
    $row = mysqli_fetch_assoc($result);
    if (!$row) {
      http_response_code(400);
      echo json_encode(array("message" => "Comments does not exist."));
      return;
    }
  }
  http_response_code(200);
  echo json_encode(mysqli_fetch_all(mysqli_query($conn, $qry), MYSQLI_ASSOC));
} else {
  $qry = "SELECT * FROM user_comment";
  echo json_encode(mysqli_fetch_all(mysqli_query($conn, $qry), MYSQLI_ASSOC));
}
