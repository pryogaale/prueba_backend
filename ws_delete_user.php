<?php
require("./conDB.php");

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$dInput = json_decode(file_get_contents("php://input"));

if (!isset($_GET["id"])) {
  http_response_code(400);
  echo json_encode(array("message" => "ID is required."));
  return;
}

$qry = "DELETE FROM user WHERE id = '" . $_GET['id'] . "'";

if(mysqli_query($conn, $qry)) {
  http_response_code(200);
  echo json_encode(array("message" => "User was deleted."));
} else {
  http_response_code(500);
  echo json_encode(array("message" => "Unable to delete user."));
}
