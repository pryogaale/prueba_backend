<?php
require("./conDB.php");

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: PUT");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$dInput = json_decode(file_get_contents("php://input"));

$qry = "SELECT * FROM user_comment where id = '$dInput->id'"; 

if ($result = mysqli_query($conn, $qry)) {
  $row = mysqli_fetch_assoc($result);
  if (!$row) {
    http_response_code(400);
    echo json_encode(array("message" => "Comment does not exist."));
    return;
  }
}

$qry = "UPDATE user_comment SET coment_text = '$dInput->comment_text', likes = '$dInput->likes' WHERE id = $dInput->id";
if (mysqli_query($conn, $qry)) {
  http_response_code(200);
  echo json_encode(array("message" => "Comment was updated."));
} else {
  http_response_code(500);
  echo json_encode(array("message" => "Unable to update comment."));
}
