<?php
require("./conDB.php");

header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$dInput = json_decode(file_get_contents("php://input"));

$qry = "SELECT * FROM user where email = '$dInput->email' and fullname = '$dInput->fullname'";

if ($result = mysqli_query($conn, $qry)) {
  $row = mysqli_fetch_assoc($result);
  if ($row) {
    http_response_code(400);
    echo json_encode(array("message" => "User already exists."));
    return;
  }
}

$qry = "INSERT INTO user (fullname, email, pass, openid) VALUES ('$dInput->fullname', '$dInput->email', '$dInput->pass','$dInput->openid')";

if (mysqli_query($conn, $qry)) {
  http_response_code(200);
  echo json_encode(array("message" => "User was created."));
} else {
  http_response_code(500);
  echo json_encode(array("message" => "Unable to create user."));
}

