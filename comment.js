$(document).ready(function() {

  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);

  loadComments();
  loadDetailsUsers();

  //load users
  function loadComments() {
    tableBody = $("table tbody");
    tableBody.empty();
    rowTable = "";
    $.ajax({
      "url": "http://localhost:8080/ws_get_comment.php?user=" + urlParams.get('id'),
      "type": "GET",
      "dataType": "json",
    }).done(function(data) {
      data.forEach((element) => {
        $.ajax({
          "url": "http://localhost:8080/ws_get_user.php?id=" + element.user,
          type: "GET",
          datatype: "json",
        }).done(function(userDetails) {
          rowTable = `<tr>
                <td>${element.id}</td>
                <th scope="row">${userDetails.fullname}</th>
                <td>${element.coment_text}</td>
                <td>${element.likes}</td>
<td><button id="showUpdateCommentModal" type="button" class="btn btn-primary" commentId="${element.id}">Update</button></td>
<td><button id="deleteComment" type="button" class="btn btn-danger" commentId="${element.id}">Delete</button></td>

</tr>`;
          tableBody.append(rowTable);
        }).fail(function(jqXHR, textStatus) { });
      });

    }).fail(function(jqXHR, textStatus) {
      rowTable = `<tr> <td colspan="6">No comments</td> </tr>`;
      tableBody.append(rowTable);
      console.log('No regreso nada');
    });
  }

  function loadDetailsUsers() {
    $.ajax({
      "url": "http://localhost:8080/ws_get_user.php",
      "type": "GET",
      "dataType": "json",
    }).done(function(data) {
      data.forEach((element) => {
        currentUserId = urlParams.get('id');
        options = `<option value="${element.id}" ${currentUserId == element.id ? "selected" : ""}>${element.fullname}</option>`;
        $('#aInputUser').append(options);
        $('#uInputUser').append(options);
      });
    }).fail(function(jqXHR, textStatus) {
    });

  }

  $("#addComment").click(function(e) {
    e.preventDefault();
    var user = $('#aInputUser').val();
    var comment = $('#aInputComment').val();
    var likes = $('#aInputLikes').val();
    $.ajax({
      "url": "http://localhost:8080/ws_post_comment.php",
      "type": "POST",
      "dataType": "json",
      "data": JSON.stringify({
        "user": user,
        "comment_text": comment,
        "likes": likes,
      }),
    }).done(function(data) {
      $("#addCommentModal").modal('hide');
      $("#alert-ok").text(data.message);
      $("#alert-ok").show();
      $("#alert-ok").fadeTo(2000, 500).slideUp(500, function() {
        $("#alert-ok").hide();
        $(':input').val('');
      });
      loadComments();
    }).fail(function(jqXHR, textStatus) {
      console.log();
      $("#addCommentModal").modal('hide');
      $("#alert-error").text(JSON.parse(jqXHR.responseText).message);
      $("#alert-error").show();
      $("#alert-error").fadeTo(2000, 500).slideUp(500, function() {
        $("#alert-error").hide();
      });
      loadComments();
    });
  });

  $(document).on("click", '#showUpdateCommentModal', function(e) {
    e.preventDefault();
    $.ajax({
      "url": "http://localhost:8080/ws_get_comment.php?id=" + $(this).attr("commentId"),
      "type": "GET",
      "dataType": "json",
    }).done(function(data) {
      $('#uInputId').val(data.id);
      $("#uInputComment").val(data.coment_text);
      $("#uInputLikes").val(data.likes);
    }).fail(function(jqXHR, textStatus) {
    });
    $("#updateCommentModal").modal('show');
  });

  $(document).on("click", "#updateComment", function(e) {
    e.preventDefault();
    $.ajax({
      "url": "http://localhost:8080/ws_put_comment.php",
      "type": "PUT",
      "dataType": "json",
      "data": JSON.stringify({
        "id": $('#uInputId').val(),
        "user": $('#uInputUser').val(),
        "comment_text": $('#uInputComment').val(),
        "likes": $('#uInputLikes').val(),
      }),
    }).done(function(data) {
      $("#updateCommentModal").modal('hide');
      $("#alert-ok").text(data.message);
      $("#alert-ok").show();
      $("#alert-ok").fadeTo(2000, 500).slideUp(500, function() {
        $("#alert-ok").hide();
      });
      loadComments()();
    });
  });

  $(document).on("click", "#deleteComment", function(e) {
    var deleteComment = confirm("Are you sure?");
    if (deleteComment) {
      $.ajax({
        "url": "http://localhost:8080/ws_delete_comment.php?id=" + $('#deleteComment').attr("commentId"),
        "type": "DELETE",
        "dataType": "json",
      }).done(function(data) {
        loadComments()();
      });
    }
  });

});
